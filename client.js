import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import {createLogger} from 'redux-logger'
import promise from 'redux-promise-middleware'
import reducers from './src/reducer'

import Wrapper from './src/app'
import promiseMiddleware from 'redux-promise-middleware';


const middleware = applyMiddleware(promise(), thunk, createLogger())
const store = createStore(reducers, middleware);
 
class App extends React.Component {
  render() {
    return (
    	<Provider store={store}>
				<Wrapper />
			</Provider>
    )
  }
}
window.__myapp_wrapper = document.getElementById('root')
ReactDOM.render(
	<App />, 
	window.__myapp_wrapper
);