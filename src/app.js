import React from 'react'
import {connect} from 'react-redux'

import Popup from './components/popup'
import Button from './components/button'

import './style.css'

class Wrapper extends React.Component {
	render() {
		const popup = (this.props.popupActive) ? <Popup /> : null
    return (
    	<div>
				<Button/>
    		{popup}
    	</div>
    )
  }
}

export default connect(
	state => ({popupActive : state.popup.visible})
)(Wrapper)