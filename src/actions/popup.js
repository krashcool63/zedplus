export function openPopup () {
    return dispatch => {
        sessionStorage.setItem('popupVisible', 'true')
        dispatch({
            type: 'POPUP_OPEN'
        })
    }
}
export function closePopup () {
    return dispatch => {
        sessionStorage.setItem('popupVisible', '')
        dispatch({
            type: 'POPUP_CLOSE'
        })
    }
}

export function getList () {
    return dispatch => {
        dispatch({
            type: 'LIST',
            payload: fetch('http://cdn.music.beeline.ru/api/v2/music/clips/popularList')
                .then( 
                    response => response.json(),
                    error => console.log('An error occured.', error)
                )
                .then( json => {
                    return json.result.items
                })
        })
    }
  
}

export function delList () {
    return {
        type: 'DEL_LIST'
    }
}
