import { combineReducers } from 'redux'
import popup from './components/popup/reducer'

const todoApp = combineReducers({
  popup
})

export default todoApp