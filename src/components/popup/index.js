import React from 'react'
import ReactDOM from 'react-dom';
import * as Actions from '../../actions/popup'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {Item} from './item'
import {Loader} from '../loader'

import css from './style.css';

class Popup extends React.Component {
	constructor(props){
    super(props);
    this.escFunction = this.escFunction.bind(this);
	}
  escFunction(event){
    if(event.keyCode === 27) {
			this.props.actions.closePopup()
    }
  }
  componentDidMount(){
		document.addEventListener("keydown", this.escFunction, false);
		window.__myapp_wrapper.addEventListener('click', this.handleDocumentClick)
		this.props.actions.getList()
  }
  componentWillUnmount(){
		document.removeEventListener("keydown", this.escFunction, false);
		window.__myapp_wrapper.removeEventListener('click', this.handleDocumentClick)
	}
	handleDocumentClick = (e) => {
    const area = ReactDOM.findDOMNode(this.refs.area);
    (!area.contains(e.target)) && this.props.actions.closePopup()
  }
  render() {
		const {visible, isLoading, data} = this.props.popup
		const items = (data.length) ?
			<div className='items'>
				{data.map(el => 
					<Item key={el.id} {...el.snippet}/>
				)}
			</div>
		: null
		const loader = (isLoading) ? <Loader /> : null 
    return (
    	<div className='popup'>
    		<div className='popup_block' ref='area'>
					<h1>Вот они наши записи</h1>
					{items}
					{loader}
				</div>
    	</div>
    )
	}
}

export default connect(
	state => ({popup : state.popup, articles: state.articles}),
	dispatch => ({ 
		actions: bindActionCreators(Actions, dispatch), 
	})
)(Popup)