const initialState = {
  visible: (sessionStorage.getItem('popupVisible') == 'true') ? true : false,
  isLoading: false,
  data: []
}

const popup = (state = initialState, action) => {
  switch (action.type) {
    case 'LIST_PENDING':
      return Object.assign({}, state, {isLoading: true} )
    case 'LIST_FULFILLED':
      return Object.assign({}, state, {isLoading: false, data: action.payload} )
    case 'POPUP_OPEN':
      return Object.assign({}, state, {visible: true} )
    case 'POPUP_CLOSE':
      return Object.assign({}, initialState, {visible: false} )
    default:
      return state
  }
}

export default popup