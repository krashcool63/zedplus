import React from 'react'

export const Item = (props) => {
    return (
        <div className='item'>
            <div className='item_title'>
                {props.title}
            </div>
        </div>
    )
}