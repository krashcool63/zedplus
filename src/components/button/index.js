import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../../actions/popup'

import './style.css'

class Button extends React.Component {
	render() {
		return (
			<button className="action-button shadow animate green" onClick={this.props.actions.openPopup}>
				Нажми скорее на меня
			</button>
		)
	}
}

export default connect(
	state => ({form : state.form, articles: state.articles}),
	dispatch => ({ 
		actions: bindActionCreators(Actions, dispatch), 
	})
)(Button)